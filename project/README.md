#Технологии разработки программного обеспечения

Проект по предмету технологии разработки ПО на тему "Анализ объемов торгов на товарных, валютных, криптовалютных биржах"

###Участники проекта:
- Алтухов Н. А. - Lead Developer
- Борисов А. Р. - System Administrator
- Томар К. В. - Project Manager

###Руководитель проекта:
- Филлипов Е. В. - к.т.н., доцент